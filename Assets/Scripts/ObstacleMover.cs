﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMover : MonoBehaviour
{
    public float speed = -2;
    public float leftBound = -12;

    GameManager GameManagerRef;

    void Start()
    {
        GameManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Move the obstacle if the game is not over
        if (GameManagerRef.GetComponent<GameManager>().GameOver == false)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }

        //Delete out of bounds obstacles
        if (transform.position.x <= leftBound)
        {
            Destroy(gameObject);
        }
    }
}
