﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] objects;

    public bool GameOver = false;

    //Called when the player dies
    public void DestroyGameObjects()
    {
        //Set certain game objects not active
        for (int i = 0; i < objects.Length; i++)
        {
            objects[i].SetActive(false);
        }

        //Set that the game is over
        GameOver = true;
    }
}
