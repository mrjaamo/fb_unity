﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UI_Manager : MonoBehaviour

{

    public GameObject MainMenu;
    public GameObject DeathMenu;
    public GameObject ScoreSystem;
    public GameObject HighScoreSystem;
    public int TheScore = 0;
    public int TheHighScore = 0;


    // Start is called before the first frame update
    void Start()
    {
        MainMenu.SetActive(true);
        DeathMenu.SetActive(false);
        ScoreSystem.SetActive(false);

        //Get the highscore value
        TheHighScore = PlayerPrefs.GetInt("highscore");
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        ScoreSystem.GetComponent<Text>().text = "Score: " + TheScore;
        HighScoreSystem.GetComponent<Text>().text = "Highscore: " + TheHighScore;
    }

    public void PlayGame()
    {
        //Resets the current score before playing
        TheScore = 0;
        MainMenu.SetActive(false);
        DeathMenu.SetActive(false);
        ScoreSystem.SetActive(true);
        HighScoreSystem.SetActive(false);
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("QUIT");
    }

    public void GameOver()
    {
        DeathMenu.SetActive(true);
        HighScoreSystem.SetActive(true);
    }

    public void LoadMainMenu()
    {
        DeathMenu.SetActive(false);
        MainMenu.SetActive(true);
        Destroy(this.gameObject);
        SceneManager.LoadScene(0);
  
    }

    public void AddScore()
    {
        TheScore += 1;
    }

    public void AddToHighScore()
    {
        TheHighScore = TheScore;
        Debug.Log("The new highscore:" + TheHighScore);

        //Save to the player preferences
        PlayerPrefs.SetInt("highscore", TheHighScore);
    }

}