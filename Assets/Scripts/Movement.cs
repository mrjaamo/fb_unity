﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public int JumpForce = 400;

    private Rigidbody2D Rigidbody;

    private Animator PlayerAnimation;

    private GameObject canvas;

    bool IsDead = false;

    GameManager GameManagerRef;

    /* AUDIO */
    public AudioSource PlayerSounds;
    public AudioClip JumpSound;
    public AudioClip HitSound;

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody = GetComponent<Rigidbody2D>();

        PlayerAnimation = GetComponent<Animator>();

        canvas = GameObject.FindGameObjectWithTag("UIManager");
        GameManagerRef = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Jump function if player is not dead
        if (Input.GetKeyDown(KeyCode.Space) && IsDead == false)
        {
            //Add force for the jump
            Rigidbody.AddForce(Vector2.up * JumpForce);

            //Play the jump animation
            PlayerAnimation.Play("Jump");

            //Play the jumping sound
            PlayerSounds.PlayOneShot(JumpSound, 1.0f);
        }
    }

    //Detect collisions
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Obstacle") == true)
        {
            //Play the death animation
            PlayerAnimation.SetBool("IsDead", true);

            //Play the hit sound when the player is hit (death)
            PlayerSounds.PlayOneShot(HitSound, 1.0f);

            //Display the death screen.
            canvas.GetComponent<UI_Manager>().GameOver();
            //Set the player dead.
            IsDead = true;
            //Call the GameManager to destroy all the objects so the movement stops
            GameManagerRef.GetComponent<GameManager>().DestroyGameObjects();

            //If the player reaches the high score when he's dead
            if (canvas.GetComponent<UI_Manager>().TheScore > canvas.GetComponent<UI_Manager>().TheHighScore)
            {
                canvas.GetComponent<UI_Manager>().AddToHighScore();
            }
        }

        //Add score when the player passes the tree
        if (other.gameObject.CompareTag("Score") == true)
        {
            canvas.GetComponent<UI_Manager>().AddScore();
        }
    }
}