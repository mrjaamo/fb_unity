﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject[] ObstaclePrefab;
    private Vector2 SpawnPosition;

    public GameObject jungspawn;
    public GroundMovement ground;

    private float StartDelay = 1.4f;
    private float RepeatRate = 5f;

    private float MinHeight = 4f;
    private float MaxHeight = 10f;

    public int obsID;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObstacle", StartDelay, RepeatRate);

        jungspawn = GameObject.Find("JungleSpawner");
        ground = jungspawn.GetComponent<GroundMovement>();

        SpawnPosition = this.transform.position;

    }

    //Spawn the obstacles
    void SpawnObstacle()
    {
        //If the object is active, spawn obstacles
        if (gameObject.activeSelf == true)
        {
            //Random height for the obstacles
            float RandomHeight = Random.Range(MinHeight, MaxHeight);

            //Spawn the object
            GameObject SpawnedObject = Instantiate(ObstaclePrefab[obsID], SpawnPosition, ObstaclePrefab[obsID].transform.rotation);

            //Scale the object
            SpawnedObject.transform.localScale = new Vector2(1, RandomHeight);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Switch for changing the obstacle ID, depending on the background (detected with tag)
        switch(other.gameObject.tag)
        {
            case "JungleBackground":
                obsID = 0;
                break;
            case "SandBackground":
                obsID = 1;
                break;
            case "NightBackground":
                obsID = 2;
                break;
        }
    }
}