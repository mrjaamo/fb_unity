﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float speed = -5;
    public float leftBound = -12;

    // Update is called once per frame
    void Update()
    {
        //Move the enemy
        transform.Translate(Vector3.right * Time.deltaTime * speed);

        //Delete out of bounds enemies
        if (transform.position.x <= leftBound)
        {
            Destroy(gameObject);
        }
    }
}
