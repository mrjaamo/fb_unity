﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] EnemyPrefabs;

    private Vector2 SpawnPosition = new Vector2(0, 0);

    private float StartDelay = 4f;
    private float RepeatRate = 1.8f;

    float SpawnLocationRandomized = 0f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemy", StartDelay, RepeatRate);
    }

    //Spawn the enemies
    void SpawnEnemy()
    {
        //Randomize spawn location (on y axis) for the enemies
        SpawnLocationRandomized = Random.Range(0f, 4.4f);

        //Sets the spawn position
        SpawnPosition = new Vector2(20, SpawnLocationRandomized);

        //Spawn a random enemy
        Instantiate(EnemyPrefabs[Random.Range(0, EnemyPrefabs.Length - 1)], SpawnPosition, Quaternion.identity);
    }
}